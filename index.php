<?php

require_once("animal.php");
require_once("frog.php");
require_once("ape.php");
$sheep = new Animal("shaun");

echo "<h3>Tugas Hari-4</h3>";
echo "<br>";
echo "Name : " . $sheep->name; // "shaun"
echo "<br>";
echo "legs :" . $sheep->legs; // 4
echo "<br>";
echo "cool blooded : " . $sheep->coolBlooded; // "no"

echo "<br>";
echo "<br>";

$frog = new Frog("buduk");
echo "Name : " . $frog->name;
echo "<br>";
echo "legs :" . $frog->legs;
echo "<br>";
echo "cool blooded : " . $frog->coolBlooded;
echo "<br>";
echo "jump : " . $frog->jump;
echo "<br>";

echo "<br>";


$ape = new Ape("kera sakti");
echo "Name : " .  $ape->name;
echo "<br>";
echo "legs :" .  $ape->legs;
echo "<br>";
echo "cool blooded : " .  $ape->coolBlooded;
echo "<br>";
echo "jump : " .  $ape->yell;
echo "<br>";
